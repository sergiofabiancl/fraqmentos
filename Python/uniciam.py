# pip3 install --user requests
# python3 uniciam.py > results.csv

import requests
import json


URLVU = "__URL-API-VU__"

def check_ciam_unification(rut):
    headers = {
        "Content-Type" : "application/json",
        "X-api-key" : "__API-KEY__",
    }
    data = {
        "fields": [{
            "claimName": "Document",
            "valueField": rut
        }]
    }
    data = json.dumps(data)
    req = requests.post(URLVU + "__ENDPOINT__", headers=headers, data=data).text
    #req = json.loads(req)[0]["userAccountResponseList"]

    ciamIds = []
    for item in json.loads(req):
        user = item["userAccountResponseList"]
        if len(user) > 1:
            ciamId = user[0]["ciamId"]
            #for account in user:
            #    if ciamId != account["ciamId"]:
            #        return ""
            return (True, ciamId)
        else:
            for u in user:
                if u['serviceProviderName'] == "__PROVIDER-BU__":
                    ciamId = u["ciamId"]
                    if ciamId not in ciamIds:
                        ciamIds.append(ciamId)
    ciamIds = (",".join(ciamIds)).replace("'", "")
    return (False, ciamIds)

ruts = [
    "123456789",
    "123456789",
    "123456789"
]

print("RUT,SI,NO,CIAMID,CIAMID __PROVIDER-BU__")
for rut in ruts:
    r = rut[:-1]
    d = rut[-1]
    newrut = r + "-" + d
    v, ciamId = check_ciam_unification(newrut)
    if v:
        print("{},X, ,{}, ".format(newrut, ciamId))
    else:
        print("{}, ,X, ,{}".format(newrut, ciamId))
