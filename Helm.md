#### HELM

## Rollback
* Buscar y seleccionar contexto de ambiente
kubectl config get-contexts
kubectl config use-context "contexto"
* Listar despliegues/charts
helm3 ls -n uat
* Listar historial del chart
helm3 history "chartName" -n uat
* Ejecutar rollback a una version
helm3 rollback "chartName" 22 -n uat

## Obtener informacion
* Ver variables entregadas a deploy
helm3 get values "name" -n "namespace"

## Instalacion
* Listar instalaciones de helm
helm list
* Agregar libreria de charts
helm repo add stable https://charts.helm.sh/stable
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add autoscaler https://kubernetes.github.io/autoscaler
* Listar librerias de charts
helm repo list
* Quitar libreria de charts
helm repo remove bitnami
helm repo remove autoscaler
* Actualizar lista de librerias
helm repo update
* Buscar en libreria
helm search repo bitnami
helm search repo stable
* Instalar un chart
helm install my-freeradius truecharts/freeradius --version 1.7.18
helm install bitnami/wordpress --generate-name
helm install apache bitnami/apache 
helm install happy-panda bitnami/wordpress
* Desinstalar un release
helm uninstall happy-panda
helm uninstall my-freeradius

# Previamente cargar kubeconfig y contexto que corresponda
export GOOGLE_APPLICATION_CREDENTIALS=gke_prod.yml
export KUBECONFIG=kubeconf_int_prod
kubectl config get-contexts



