## GCLOUD
* Conectar cuenta ya configurada (Interactivo)
gcloud auth login

* Conectar cuenta de servicio
gcloud auth activate-service-account "service-account" --key-file="key.json" --project "project-id"

* Configurar kubectl para conectarse a gcloud
gcloud container clusters get-credentials NOMBRE_DEL_CLUSTER --zone "ubicacion" --project "project-id"

* Validar la SA que está usando un cluster GKE
gcloud container clusters describe NOMBRE_DEL_CLUSTER --format='value(nodeConfig.serviceAccount)' --region=us-east4 --project PROYECTO_GCP

* Cambiar proyecto seleccionado
gcloud config set project NOMBRE_PROYECTO --quiet

* Conexion a bastion IAP
gcloud compute ssh NOMBRE_VM --zone us-east4-c --tunnel-through-iap -- -N -L PUERTO_ORIGEN:127.0.0.1:PUERTO_DESTINO & sleep 10

## ------------- PUBSUB  -------------
* Para obtener listado de suscripciones:
gcloud alpha pubsub subscriptions list
gcloud pubsub subscriptions list --format yaml

* Para describir una subscription:
gcloud pubsub subscriptions describe SUBSCRIPCION

* Para pullear un mensaje de subscriptor
gcloud pubsub subscriptions pull SUBSCRIPCION --limit=1 --format="json(message.attributes)"

* (tambien se puede obtener la data del mensaje pero hay que desencriptarla segun pagina https://stackoverflow.com/questions/43914026/gcloud-beta-pubsub-subscriptions-pull-format)
gcloud pubsub subscriptions pull projects/NOMBRE_PROYECTO/subscriptions/NOMBRE_SUBSCRIPCION --limit=1 --format="json(message.attributes.tenantId,message.data.decode(\"base64\").decode(\"utf-8\"))"

* Para updatear un subscriptor.
gcloud pubsub subscriptions update SUBSCRIPCION --update-labels=[KEY=VALUE,…]

* Para describir un topico:
gcloud pubsub topics describe TOPICO

* Para updatear labels:
gcloud pubsub topics update TOPICO --update-labels=[KEY=VALUE,…]

## Workload Identity
* Crear cuenta de servicio en kubernetes
kubectl create serviceaccount NOMBRE_KSA \
    --namespace NAMESPACE

* Crear cuenta de servicio en gcloud
gcloud iam service-accounts create NOMBRE_GSA \
    --project=NOMBRE_PROYECTO

* Asignar rol de pubsub subscriber
gcloud projects add-iam-policy-binding NOMBRE_PROYECTO \
    --member "serviceAccount:NOMBRE_GSA@NOMBRE_PROYECTO.iam.gserviceaccount.com" \
    --role "roles/pubsub.subscriber"

* Vicnulacion de cuentas via IAM
gcloud iam service-accounts add-iam-policy-binding NOMBRE_GSA@NOMBRE_PROYECTO.iam.gserviceaccount.com \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:NOMBRE_PROYECTO.svc.id.goog[NAMESPACE/NOMBRE_KSA]"

* Hacer anotacion/vinculacion en el cluster k8s
kubectl annotate serviceaccount NOMBRE_KSA \
    --namespace NAMESPACE \
    iam.gke.io/gcp-service-account=NOMBRE_GSA@NOMBRE_PROYECTO.iam.gserviceaccount.com

## Probar Secretos con External Secret Operator

apiVersion: v1
kind: Pod
metadata:
  name: workload-identity-test
  namespace: NAMESPACE
  annotations:
    traffic.sidecar.istio.io/excludeOutboundIPRanges: 169.254.169.254/32
spec:
  containers:
  - image: google/cloud-sdk:slim
    name: workload-identity-test
    command: ["sleep","infinity"]
  serviceAccountName: NOMBRE_KSA
  nodeSelector:
    iam.gke.io/gke-metadata-server-enabled: "true"

* En el pod usar el comando
gcloud secrets versions access 1 --secret="secretisimo"


## Obtencion de data
* Lista de versiones disponibles para el cluster
gcloud container get-server-config --flatten="channels" --filter="channels.channel=STABLE" --format="yaml(channels.channel,channels.validVersions)" --region us-east4


