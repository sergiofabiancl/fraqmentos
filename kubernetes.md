#### K8S
# Alias
alias k='kubectl'

# Cambiar contexto
* Listar contextos
kubectl config get-contexts
* Seleccionar contexto
kubectl config use-context NOMBRE_CONTEXTO

# Obtener datos
kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}'

kubectl -n NAMESPACE get deployment DEPLOY_NAME -o jsonpath='{.spec.template.spec.containers[*].resources.limits.cpu}'
kubectl -n NAMESPACE get deployment DEPLOY_NAME -o jsonpath='{.spec.template.spec.containers[*].resources.limits.memory}'
kubectl -n NAMESPACE get deployment DEPLOY_NAME -o jsonpath='{.spec.template.spec.containers[*].resources.requests.cpu}'
kubectl -n NAMESPACE get deployment DEPLOY_NAME -o jsonpath='{.spec.template.spec.containers[*].resources.requests.memory}'

kubectl -n NAMESPACE get hpa DEPLOY_NAME -o jsonpath='{.spec.maxReplicas}'
kubectl -n NAMESPACE get hpa DEPLOY_NAME -o jsonpath='{.spec.minReplicas}'

# Obtener informacion de un secreto
kubectl get secret NOMBRE-DEL-SECRETO -n NAMESPACE -o go-template='
{{range $k,$v := .data}}{{printf "%s: " $k}}{{if not $v}}{{$v}}{{else}}{{$v | base64decode}}{{end}}{{"\n"}}{{end}}'

# Agregar cuenta docker como secreto (En directorio Laboratorio)
kubectl create secret -n testing docker-registry docker-pull-prd \
  --docker-server=gcr.io \
  --docker-username=_json_key \
  --docker-password="$(cat docker-pull-prd.json)"

kubectl patch sa default -n testing -p '{"imagePullSecrets": [{"name": "docker-pull-prd"}]}'

# Generar YAMLs
kubectl get hpa DEPLOY_NAME -n NAMESPACE -o yaml > ARCHIVO.yaml
kubectl get deploy DEPLOY_NAME -n NAMESPACE -o yaml > ARCHIVO_HPA.yaml

# Exponer un deployment en el equipo local
kubectl port-forward deployment/argocd-server 8080:8080 -n argocd

# Levantar pod multitool
* single pod
kubectl run multitool --image=praqma/network-multitool -n monitoring
* deployment
kubectl create deployment multitool --image=praqma/network-multitool -n monitoring
* Conectarse shell
kubectl exec multitool --stdin --tty /bin/bash -n monitoring


# Otros
* Listar objetos
kubectl get <>
kubectl get namespaces --show-labels
* Seleccionar namespace
kubectl config view
kubectl config set-context --current --namespace=dev
* Desplegar
kubectl create -f namespace.yaml 
kubectl create deployment radius --image=sgarcescl/radedufai:1.2.1  -n=dev --replicas=2 --dry-run=client -o yaml > radius.yaml
kubectl create deployment web --image=sgarcescl/sgarces_web:1.1-nginx --replicas=3 --dry-run=client -o yaml > deployment.yaml
* Aplicar manifiesto
kubectl apply -f namespace.yaml 
* Editar despliegue
kubectl edit deployment.v1.apps/sgarces-deployment
* Borrar deployment
kubectl delete deployment sgarces-deployment
* Crear servicio
kubectl expose deployment sgarces-deployment --type=LoadBalancer --port=80
* Borrar servicio
kubectl delete service hello-node
* Conectar shell a pod
kubectl exec RUNNER --stdin --tty /bin/bash -n gitlab-runner


# 
* Obtener detalles de pods especifico
kubectl describe pod NOMBRE_POD -n monitoring
* Obtener logs de un pod
kubectl logs -f loki-read-bf446f5d6-6mnjv -n monitoring