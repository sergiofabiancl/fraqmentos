#### TERRAFORM
* 
terraform import google_service_account.my_sa projects/<project-id>/serviceAccounts/<email>
* 
terraform apply -target=module.cloudtask_queue_service_provider plan.tfplan

## TFSTATE
* 
terraform state rm some.resource.to.remove