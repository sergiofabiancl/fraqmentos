## Curl
* Conocer mi ip por consola
curl https://www.miip.cl | grep -i "mi ip es"

## SED
* Reemplazar text1 por text2 en archivo.txt
sed -i "s/text1/text2/g" archivo.txt

## SSH (Multiple)
* Generar clave publica
ssh-keygen -o -t rsa -b 4096 -C "IDENTIFICADOR"

* Crear archivo config
~~~
Host           gitlab.personal
HostName       gitlab.com
IdentityFile   C:\Users\sergiogarces\.ssh\id_rsa_2
User           sergiogarces
~~~

* Agregar llave a repo
cat id_rsa.pub

## Python environments
* Crear environment
python3 -m venv environment
* Elegir version de python
pyenv local <version>
* Activar environment
source environment/bin/activate
