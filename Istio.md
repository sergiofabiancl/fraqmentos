#### ISTIO
* Instalar istio desde helm
helm template install/kubernetes/helm/istio --name istio --namespace istio-system > $HOME/istio.yaml
kubectl create namespace istio-system
kubectl apply -f $HOME/istio.yaml
* Inyeccion de envoy
kubectl label namespace default istio-injection=enabled

## Revision de certificados
* Listar contextos
kubectl config get-contexts
* Seleccionar contexto
kubectl config use-context "nombre_contexto"
* Mostrar informacion del certificado
kubectl -n "namespace" get secret "secreto" -o yaml
* enviar a un archivo
echo "fragmento_a_decodificar_del_secreto" | base64 -d > cert.pem
* Decodificacion para obtener fecha de termino
openssl x509 -enddate -noout -in cert.pem

