#### GIT

## Merge Local
* Traer cambios desde ambiente previo
git fetch origin
git checkout -b "release2/testing" "origin/release2/testing"
* Cambiar a rama destino
git fetch origin
git checkout staging/uat
* Hacer merge desde rama previa
git merge --no-ff "release2/testing"
* Agregar cambios y comentario
git add .
git commit
* Subir cambios
git push origin staging/uat

## Configurar datos de usuario del repo
git config user.name "Sergio Fabian"
git config user.email email@correo.com
## Configurar URL remota
git remote -v
git remote set-url origin https://gitlab.com/proyecto.git
# Listar todas ramas
git branch -a
# Borrar una rama
git branch -d dev
# Cambiar rama
git checkout qa
# Descargar rama remota a nueva local
git checkout -b "local" "origin/remota"
# Subir cambios sin pipeline
git push -o ci.skip

## Ejecutar un reset hard
* Elegir rama
git checkout staging/uat
* Traer cambios
git pull --all
* Hacer reset al commit SHA
git reset --hard commmitSHA
* Subir cambios forzados a la rama
git push -f origin staging/uat
