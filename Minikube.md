#### MINIKUBE
* Arrancar nodo minikube
minikube start --cpus 4 --memory 6144
* Correr tunnel (toma consola)
minikube tunnel --cleanup
* Listar servicios
minikube service list
* Detener nodo minikube
minikube stop
* Borrar nodo minikube
minikube delete