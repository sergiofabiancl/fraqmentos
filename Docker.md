#### DOCKER
* Login en docker hub
docker login -u sgarcescl
* Crear imagen
docker build -t sgarcescl/radedufai:1.2.1 -f radius/Dockerfile "radius"
docker build --tag sgarces_app -f node/Dockerfile "node"
* Subir imagen al hub
docker push sgarcescl/radedufai:1.2.1
* Arrancar maquina creada
docker run -d --name edufai -p 1812-1813:1812-1813/udp sgarcescl/radedufai:1.2.1
docker run -d --name app -p 3000:3000 sgarces_app:latest
* Listar imagenes activas
docker ps -a
* Detener y Borrar contenedor 
docker stop 4684cdb9e6f4
docker rm 4684cdb9e6f4
* Borrar imagen guardada
docker rmi sgarcescl/radedufai:1.0
* Descargar imagen
docker pull freeradius/freeradius-server